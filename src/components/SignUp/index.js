import {Component} from "react"
import Steps from "../Steps"

import "./signup.css"





class SignUp extends Component{
    state = {
        firstName:this.props.signUpValues.firstName,
        firstNameErr:"",
        lastName:this.props.signUpValues.lastName,
        lastNameErr:"",
        email:this.props.signUpValues.email,
        emailErr:"",
        DOB:this.props.signUpValues.DOB,
        DOBErr:"",
        address:this.props.signUpValues.address,
        addressErrorMsg:""
    }

    // Updating Form inputs

    updateFirstName = (event)=>{
        this.setState({firstName:event.target.value})
    } 

    updateLastName = (event)=>{
        this.setState({lastName:event.target.value})
    }
 
    updateDateOfBirth = (event)=>{
        this.setState({
            DOB:event.target.value
        })
    }

    updateEmail = (event)=>{
        this.setState({
            email:event.target.value
        })
    }

    updateAddressValue = (event)=>{
        this.setState({address:event.target.value})
    }


    submitForm = (event)=>{
        event.preventDefault()
        const {firstName,lastName,email,DOB,address} = this.state
        
        // Validating form inputs

        firstName.trim() === "" ? this.setState({firstNameErr:"Enter a valid first name*"}) : this.setState({firstNameErr:""}) 
        lastName.trim() === "" ? this.setState({lastNameErr:"Enter a valid last name*"}) :  this.setState({lastNameErr:""})
        email === "" ?  this.setState({emailErr:"Enter a valid email-id*"}) : this.setState({emailErr:""})
        DOB === "" ? this.setState({DOBErr:"Select a Valid date*"}) : this.setState({DOBErr:""})
        address.trim() === "" ? this.setState({addressErrorMsg:"Enter a valid address*"}) : this.setState({addressErrorMsg:""})
        
        // Navigating to Message Section if form is valid

        if(firstName !== "" && lastName !== "" && email !== "" && DOB !== "" && address !== ""){
            const {statusNext} = this.props
            const {firstName,lastName,DOB,email,address} = this.state
            statusNext({
                firstName,
                lastName,
                DOB,
                email,
                address,
            })
        }

    }


    render(){
        const {firstName,firstNameErr,addressErrorMsg,lastName,emailErr,lastNameErr,email,DOB,DOBErr,address} = this.state
        const {componentStatus} = this.props
        return(
            <div className="bg-signup-container">
                <div className="image-container">
                    <img className="image" src="https://i.pinimg.com/736x/fa/dd/a9/fadda9a754c934aeb5c33becee4e5d68.jpg" alt=""/>
                </div>
                <div className="forms-steps-container">
                    <Steps componentStatus={componentStatus}/>
                        <form className="bg-form-container" onSubmit={this.submitForm}>
                            <div>
                                <span>Step1/3</span>
                                <h1>Sign UP</h1>
                            </div>
                            <div className="input-main-container">
                                <div className="inputs-container">
                                    <label htmlFor="firstName">First Name</label>
                                    <input className="signup-input" id="firstName" type="text" value={firstName} onChange={this.updateFirstName} />
                                    <span className="error-msg">{firstNameErr}</span>
                                </div>
                                <div className="inputs-container">
                                    <label htmlFor="lastName">Last Name</label>
                                    <input className="signup-input" id="lastName" type="text" value={lastName} onChange={this.updateLastName}/>
                                    <span className="error-msg">{lastNameErr}</span> 
                                </div>                           
                            </div>
                            <div className="input-main-container">
                                <div className="inputs-container">
                                    <label htmlFor="dateOfBirth">Date of Birth</label>
                                    <input className="signup-input" id="dateOfBirth" type="date" value={DOB} onChange={this.updateDateOfBirth}/>
                                    <span className="error-msg">{DOBErr}</span> 
                                </div>    
                                <div className="inputs-container">
                                    <label htmlFor="emailId">Email Address</label>
                                    <input className="signup-input" id="emailId" type="email" value={email} onChange={this.updateEmail}/>
                                    <span className="error-msg">{emailErr}</span> 
                                </div> 
                            </div>
                            <div className="inputs-container">
                                <label htmlFor="address">Address</label>
                                <input id="address" value={address} type="text" onChange={this.updateAddressValue}/>
                                <span className="error-msg">{addressErrorMsg}</span>
                            </div>
                    
                            <button id="firstStep" type="submit">
                                Next Step
                            </button>
                        </form>
                </div>
            </div>
        )
    }   
}

export default SignUp
