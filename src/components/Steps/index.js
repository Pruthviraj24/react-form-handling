import { BsCheckLg } from 'react-icons/bs'
import "./steps.css"


const Steps = (props)=>{
    const {componentStatus} = props

    
    let signUpStatus = "inactive"
    let messageStatus = "inactive"
    let checkboxStatus = "inactive"

    // Updating active status of Navigation bar

    if(componentStatus === "activeSignUp"){
        signUpStatus = "active"
    }if(componentStatus === "activeMessage"){
        signUpStatus = "completed"
        messageStatus = "active"
    }if(componentStatus === "activeCheckbox"){
        signUpStatus = "completed"
        messageStatus = "completed"
        checkboxStatus = "active"
    }


        return(
            <>
                <div className="bg-container">
                    <div className="nav-item-container">
                        <h2 className={`${signUpStatus} headers`}>
                            {signUpStatus === "completed" ?   <BsCheckLg className='checkIcon'/> : 1}
                        </h2>
                        <p className="activeSpan">Sign Up</p>
                    </div>
                    <div className="nav-item-container">
                        <h2 className={`${messageStatus} headers`}>
                            {messageStatus === "completed" ?   <BsCheckLg  className='checkIcon'/> : 2}
                        </h2>
                        <p className="activeSpan">Message</p>
                    </div>
                    <div className="nav-item-container">
                        <h2 className={`${checkboxStatus} headers`}>
                            {checkboxStatus === "completed" ?   <BsCheckLg  className='checkIcon'/> : 3}
                        </h2>
                        <p className="activeSpan">Checkbox</p>
                    </div> 
                </div>
            <hr/>
        </>
    )
} 

export default Steps