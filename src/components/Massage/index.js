import { Component } from "react";
import Steps from "../Steps";
import "./message.css"


class Message extends Component{
    state={message:this.props.messageValue,errorMsg:""}


    // Navigating back to sign-up section
    navBack = ()=>{
        const {msgStatusBack} = this.props
        msgStatusBack()
    }

    // Navigating to checkbox section 
    navNext=()=>{
        const {msgStatusNext} = this.props 
        const {message} = this.state

        if(message.trim().length < 10){
            this.setState({errorMsg:"Message should be minimum of 10 characters*"})
        }else{
            msgStatusNext(message)
        }
        
    }

    // updateing textarea content
    
    updateTextArea = (event)=>{
        this.setState({message:event.target.value})
    }

    render(){
        const {message,errorMsg} = this.state
        const {componentStatus} = this.props

        return(
            <section className="bg-message-container">
                <div>
                    <img className="message-image" src="https://i.pinimg.com/736x/cd/97/1b/cd971b4323226fd85ff7b4e4fb744229.jpg" alt=""/>
                </div>
                <div className="message-container">
                    <div>
                        <Steps componentStatus={componentStatus}/>
                    <div>
                        <p>Step 2/3</p>
                        <h1>Message</h1>
                    <div>
                        <div className="textarea-container">
                            <label htmlFor="message">Message</label>
                            <textarea onChange={this.updateTextArea} value={message} id="message"></textarea>
                            <span className="message-error">{errorMsg}</span>
                        </div>
                        <div className="meassage-radio-container">
                            <input id="numberOne" name="choice" type="radio" checked={true}/>
                            <label htmlFor="numberOne">The number one choice</label>
                            <input id="numberTwo" name="choice" type="radio" />
                            <label htmlFor="numberTwo">The Number two choice</label>
                        </div>
                    </div>
                    </div>
                        <button className="message-back-step-button" onClick={this.navBack} type="button">
                            Back
                        </button>
                        <button className="message-next-step-button" onClick={this.navNext} type="button">
                            Next Step
                        </button>
                    </div>
                </div>
            </section>
        )
    }   
}

export default Message