import { Component } from 'react';
import SignUp from '../SignUp';
import Message from "../Massage"
import Checkbox from '../Checkbox';
import './App.css';



class App extends Component{
  state = {
    signupStatus:"active",
    messageStatus:"inactive",
    checkboxStatus:"inactive",
    messageValue:"",
    submittionStatus:false,
    activeMedia:"",
    signUpValues:{firstName:"",lastName:"",email:"",DOB:"",address:""}
  }

  // Updating active section status

  updateSignUpstatus = (inputValues)=>{
    this.setState({
      signupStatus:"completed",
      messageStatus:"active",
      signUpValues:inputValues,
    })
  }

  updateMsgNext = (message)=>{
    console.log(message);
    this.setState({
      messageStatus:"completed",
      checkboxStatus:"active",
      messageValue:message
    })
  }

  updateMsgBack = ()=>{
    this.setState({
      signupStatus:"active",
      messageStatus:"inactive"
    })
  }

  updateCheckBoxSubmit = ()=>{
    this.setState({
      signupStatus:"active",
      messageStatus:"inactive",
      checkboxStatus:"inactive",
      messageValue:"",
      submittionStatus:false,
      signUpValues:{firstName:"",lastName:"",email:"",DOB:"",address:""}
    })
  }

  updateCheckBoxBack = ()=>{
    this.setState({
      checkboxStatus:"inactive",
      messageStatus:"active"
    })
  }
 
  render(){
    const {signupStatus,messageStatus,checkboxStatus,signUpValues,messageValue} = this.state

    return(
      <div className='bg-main-container'>
          {signupStatus === "active" && <SignUp componentStatus={`${signupStatus}SignUp`} statusNext={this.updateSignUpstatus} signUpValues={signUpValues}/>}
          {messageStatus === "active" && <Message componentStatus={`${messageStatus}Message`} msgStatusNext={this.updateMsgNext} msgStatusBack={this.updateMsgBack} messageValue={messageValue}/>}
          {checkboxStatus === "active"  && <Checkbox componentStatus={`${checkboxStatus}Checkbox`} checkBoxStatusNext={this.updateCheckBoxSubmit} checkBoxStatusBack={this.updateCheckBoxBack} />}
      </div>

    )
  }
}


export default App;
