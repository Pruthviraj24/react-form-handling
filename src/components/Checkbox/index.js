import { Component } from "react";
import Steps from "../Steps";
import {AiFillFacebook} from "react-icons/ai"
import {ImInstagram} from "react-icons/im"
import "./checkbox.css"


class Checkbox extends Component{
    state = {isSubmitted:false,instagram:"",faceBook:"",checkBoxRadio:"addOn",checkBoxMedia:[]}


    // Navigating Back to message Section
    navBack = ()=>{
        const {checkBoxStatusBack} = this.props
        checkBoxStatusBack()
    }

    // Subbmitting the form

    navNext=()=>{
        this.setState({isSubmitted:true})
    }

//    Navigating back to home-page after subbmitting the form 

    navBackToHome = ()=>{
        const {checkBoxStatusNext} = this.props
        checkBoxStatusNext()
    }


    // Changing checkBox status 

    selectFaceBook = ()=>{
        this.setState(prevState =>(
            {
                faceBook:prevState.faceBook === "" ? "changeBackground":"",
                checkBoxMedia:[]
            }))
        }

    selectInstagram = ()=>{
        this.setState(prevState=>({
            instagram:prevState.instagram === "" ? "changeBackground" : ""
        }))
    }


    // rendering Modal after subbmitting form

    renderModal = ()=>{
        
        return(
            <div className="modal-class">
                <h2>You have Registered Successfully</h2>
                <button className="continue-button" type="button" onClick={this.navBackToHome}>Continue</button>
            </div>
        )
    }

    updateRadio = (event)=>{
        this.setState({checkBoxRadio:event.target.value})
    }


    // Rendring Checkbox section

    renderForm = () =>{
        const {componentStatus} = this.props
        const {faceBook,instagram,checkBoxRadio} = this.state


        return(
            <section className="bg-checkbox-container">
            <div>
                <img className="checkBox-main-image" src="https://i.pinimg.com/736x/49/28/2e/49282eaab8caf3f8c4a05a33c21c9528.jpg" alt=""/>
            </div>
            <div className="checkbox-main-container">
                    <Steps componentStatus={componentStatus}/>
                <div>
                    <p>Step 3/3</p>
                    <h1>Checkbox</h1>
                    <div className="check-items-container">
                        <div onClick={this.selectFaceBook} className={`check-image-container ${faceBook}`}>
                            <AiFillFacebook  className="face-insta"/>
                        </div>
                        <div onClick={this.selectInstagram} className={`check-image-container ${instagram}`}>
                            <ImInstagram className="face-insta"/>
                        </div>
                    </div>
                        <div className="check-box-radio-inputs">
                            <input name="checkBox" id="add-option" type="radio" value="addOn" onChange={this.updateRadio} checked={checkBoxRadio === "addOn"}/>
                            <label htmlFor="add-option">I want to add the option</label>
                            <input name="checkBox" type="radio" id="click-on-this" value="clickOn" onChange={this.updateRadio} checked={checkBoxRadio === "clickOn"}/>
                            <label htmlFor="click-on-this">Let me click on this</label>
                        </div>
                    </div>
                        <button className="message-back-step-button"  onClick={this.navBack} type="button">Back</button>
                        <button className="message-next-step-button" onClick={this.navNext} type="button">Submit</button>
                </div>
        </section>
        )
    }


    render(){
        const {isSubmitted} = this.state
        
        return(
           <>
            {isSubmitted ? this.renderModal() : this.renderForm()}
           </>
        )
        
    }

}

export default Checkbox